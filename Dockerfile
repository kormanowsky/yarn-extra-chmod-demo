FROM node

RUN yarn set version stable

RUN useradd test-user && mkdir -p /home/test-user && chown -R test-user /home/test-user && chmod a-x /bin/chmod

USER test-user

CMD rbash
